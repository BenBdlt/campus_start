﻿var brep = 0; //Variable de bonne réponse
// var choix = 0;

function btn(numbtn){
    var color = brep+1;
    var nb = 0;
    document.getElementById('musique').width=490;document.getElementById('musique').height=250;
    document.getElementById('espace').style.padding=0;
    // document.getElementById("suivantimg").innerHTML = "<img src='flechenoir.png'/>";
    // document.getElementById("suivanttxt").innerHTML = "Question suivante";
    // alert(brep)
    // alert(numbtn)
    document.getElementById("btn-back").style = "display: block !important";
    if (numbtn == brep+1){
          //bonne réponse
            document.getElementById("resultat").innerHTML = "Bonne réponse tu avance d'une case !";
            // nb = parseInt(document.getElementById("score2").innerHTML); //permet de sauvegarder le socre de l'utilisateur
            // document.getElementById("score2").innerHTML = nb+1;
            
        
            document.getElementById("b"+numbtn).style = "background-color: limegreen";
            document.getElementById("b1").disabled = true; // on désactive les boutons pour empecher un nombre infinis de points.
            document.getElementById("b2").disabled = true;
            document.getElementById("b3").disabled = true;
    } else {
            //mauvaise reponse
            document.getElementById("resultat").innerHTML = "Mauvaise réponse ... tu recule d'une case";
            // document.getElementById("b"+numbtn).style = "background-color: firebrick";
            document.getElementById("b1").style = "background-color: firebrick"; // on désactive les boutons pour empecher un nombre infinis de points.
            document.getElementById("b2").style = "background-color: firebrick";
            document.getElementById("b3").style = "background-color: firebrick";
            document.getElementById("b"+color).style = "background-color: limegreen";
            document.getElementById("b1").disabled = true; // on désactive les boutons pour empecher un nombre infinis de points.
            document.getElementById("b2").disabled = true;
            document.getElementById("b3").disabled = true;
    }
}

function next(choix){
    var groupe = ['Carvan','Lomepal','Orelsan','Liluzi','Nekfeu'];
    var rep = [["Lone Digger - Caravan Palace","Drake - God's Plan","Linkin Park - Numb"],['Lomepal - Yeux disent','Post Malone - Psycho','Roméo Elvis - Dessert'],['Lefa - Paradise','Casseurs Flowters - Inachevés','Orelsan - Basique'],['DRAM - Broccoli','Lil Pump - ESSKEETIT','Lil Uzi Vert - XO tour Llif3'],['Vald - Gris','Nekfeu - Ma dope','S.Pri Noir - Skywalker']];
    var bon = [0,0,2,2,1];
    var gif = ["gif/caravan.gif","gif/lomepal.gif","gif/Orelsan.gif","gif/Liluzi.gif","gif/Nekfeu.gif"];
    var music = ["musique/Caravan.mp3","musique/Yeuxdisent.mp3","musique/Basique.mp3","musique/Xotour.mp3","musique/Madope.mp3"];
    //var choix = Math.floor(6*Math.random()); //entier aléatoire entre 0 et 5
    
    document.getElementById("b1").disabled = false; // on ré-active les boutons
    document.getElementById("b2").disabled = false;
    document.getElementById("b3").disabled = false;
    brep = bon[choix];
    document.getElementById("b1").value = rep[choix][0];
    document.getElementById("b2").value = rep[choix][1];
    document.getElementById("b3").value = rep[choix][2];
    document.getElementById('musique').width=1;document.getElementById('musique').height=1;
    document.getElementById('espace').style.padding="12.55%";
    document.getElementById('musique').src = gif[choix];
    // document.getElementById("suivantimg").innerHTML = "";
    // document.getElementById("suivanttxt").innerHTML = "";
    document.getElementById("resultat").innerHTML = "";
    //document.getElementById("oui").src= "";
    document.getElementById("source").src = music[choix]; //changement de la source audio puis lancement du nouvelle audio
    document.getElementById('son2').load();
    //prompt (choix); test pour vérifier [choix]
    if (choix < 5){
        choix = choix+1;
        // document.getElementById("situation").innerHTML = "Musique "+choix+"/5";
        if (choix == 5){
            choix = 0;
        }
    }
}

function redirect(btn_color) {
    if (btn_color == 'green') {
        document.getElementById('selector-pink').style = "display: none !important";
        document.getElementById('selector-green').style = "display: grid !important";
    } else {
        document.getElementById('selector-green').style = "display: none !important";
        document.getElementById('selector-pink').style = "display: grid !important";
    }
}

function submit(color_sel) {
    if (color_sel == 'green') {
        var values = Array.from(document.querySelectorAll('select[name="green-sel"] > optgroup > option:checked')).map(el => el.getAttribute('value'));
        switch(values[0]) {
            case "1" :
                // window.location.href = "https://form.dragnsurvey.com/survey/r/f03b4e95";
                window.open('https://form.dragnsurvey.com/survey/r/f03b4e95', '_blank');
                break
            case "2" :
                window.open('https://form.dragnsurvey.com/survey/r/2a994033','_blank');
                break
            case "3" :
                window.open('https://form.dragnsurvey.com/survey/r/6b780091','_blank');
                break
            case "4" :
                window.open('https://form.dragnsurvey.com/survey/r/b28bb2fb','_blank');
                break
            case "5" :
                window.open('https://form.dragnsurvey.com/survey/r/0d781294','_blank');
                break
            case "6" :
                window.open('https://form.dragnsurvey.com/survey/r/035611e5','_blank');
                break
            case "7" :
                window.open('https://form.dragnsurvey.com/survey/r/01d4d2a8','_blank');
                break
            case "8" :
                window.open('https://form.dragnsurvey.com/survey/r/63fd6e0a','_blank');
                break
            case "9" :
                window.open('https://form.dragnsurvey.com/survey/r/cee7c7ec','_blank');
                break
            case "10" :
                window.open('https://form.dragnsurvey.com/survey/r/c973beb4','_blank');
                break
            case "11" :
                window.open('https://form.dragnsurvey.com/survey/r/920848d5','_blank');
                break
            case "12" :
                window.open('https://form.dragnsurvey.com/survey/r/35998a51','_blank');
                break
        }
    } else {
        var values = Array.from(document.querySelectorAll('select[name="pink-sel"] > optgroup > option:checked')).map(el => el.getAttribute('value'));
        switch(values[0]) {
            case "1" :
                test = 'TESTTT'
                window.open('projet.html');
                break
            case "2" :
                window.open('projet2.html');
                break
            case "3" :
                window.open('projet3.html');
                break
            case "4" :
                window.open('projet4.html');
                break
            case "5" :
                window.open('projet5.html');
                break
        }
    }       
}
